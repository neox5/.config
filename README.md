# neox .config

----

# Installation

Clone repository to home folder

```shell
ssh:    git clone git@gitlab.com:cfaustmann/.config.git
https:  git clone https://gitlab.com/cfaustmann/.config.git
```

Add to container file to **.bashrc** or **.zshrc**:

```shell
# User configuration
source ~/.config/neox/neox.sh
```

## App Configs

Copy **.gitconfig** to home folder

```shell
cp ~/.config/neox/app_configs/.gitconfig ~/.gitconfig
```

Copy vscode settings to Application Support folder (macOS)

```shell
cp ~/.config/neox/app_configs/.vscode_keybindings.json /Users/<user>/Library/Application\ Support/Code/User/keybindings.json
cp ~/.config/neox/app_configs/.vscode_settings.json /Users/<user>/Library/Application\ Support/Code/User/settings.json
```

Import iterm2 profile under **Preferences > Profiles > Other Actions > Import JSON Profiles...**

## Scripts

Run **vscode_extensions.sh** once after installation:

```shell
chmod u+x ~/.config/neox/scripts/vscode_extensions.sh
~/.config/neox/scripts/vscode_extensions.sh
```

## Karabiner Elements settings and profile

~/.config should be present before Karabiner Elements installation