# Go development
export GO111MODULE=on # enable module mode for protoc
export GOROOT="/usr/local/go"
export GOBIN="${GOROOT}/bin"
export PATH="$PATH:$GOBIN"
export GOPATH="${HOME}/.go"
# for private repos
export GOPRIVATE="gitlab.com/ratetheweb"
