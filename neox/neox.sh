# CONTAINER FILE
# source ~/.config/neox/_iterm2.sh
source ~/.config/neox/_aliases.sh
source ~/.config/neox/_less_config.sh
# source ~/.config/neox/_gpg.sh
# source ~/.config/neox/_gnuutils.sh
source ~/.config/neox/_nvm.sh
source ~/.config/neox/_zsh.sh
source ~/.config/neox/_vim.sh
source ~/.config/neox/_golang.sh
source ~/.config/neox/scripts/scripts.sh
