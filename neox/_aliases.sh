# neox alias definition file

alias rm="rm -i"

# folders
alias dev="cd ~/dev"
alias gt="cd ~/dev/ghosttransfer"
alias cf="cd ~/dev/cfaustmann"
alias rep="cd ~/dev/repositories"

# k8s
alias k="kubectl"

# aws cli
alias s3="aws s3"

# mini scripts
http2 () {
  curl -sI "$1" -o/dev/null -w '%{http_version}\n'
}
