# https://stackoverflow.com/questions/35773299/how-can-you-export-the-visual-studio-code-extension-list
echo "installing vscode extensions"

code --install-extension Angular.ng-template
code --install-extension bierner.lit-html
code --install-extension esbenp.prettier-vscode
code --install-extension golang.go
code --install-extension infinity1207.angular2-switcher
code --install-extension jakethashi.vscode-angular2-emmet
code --install-extension ms-azuretools.vscode-docker
code --install-extension ms-vscode.vscode-typescript-tslint-plugin
code --install-extension natewallace.angular2-inline
code --install-extension naumovs.theme-oceanicnext
code --install-extension Nuuf.theme-hackershaze
code --install-extension PKief.material-icon-theme
code --install-extension redhat.vscode-yaml
code --install-extension shakram02.bash-beautify
code --install-extension stylelint.vscode-stylelint
code --install-extension vincaslt.highlight-matching-tag
code --install-extension wayou.vscode-todo-highlight
code --install-extension yzhang.markdown-all-in-one