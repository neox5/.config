compressvideos() {
    echo "video compression with ffmpeg"
    if [ ! -d compressed ] ; then
        echo "no directory compressed found"
        mkdir "compressed"
    fi
    
    EXT=mp4 # default value

    if [ -z $1 ]; then
        echo "<extention> argument not set"
    else
        EXT=$1
    fi
    
    echo "Script using .$EXT extention"
    
    for i in *.$EXT;
    do name=`echo "$i" | cut -d'.' -f1`
        echo "$name"
        ffmpeg -i "$i" -c:v libx264 -b:v 2.5M -maxrate 2.5M -bufsize 5M "compressed/${name}.${EXT}"
    done
}
